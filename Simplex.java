import java.util.*;

public class Simplex {
	private static final double DELTA = 0.000001;
    public boolean isMaxProblem,wasMaxProblem,ph2;
	public double[][] rhs;
	public double[][] A;
	public double[][] XN;
	public double[][] Bi;
	public double[][] Bib;
	public double[][] cT;
	public double[][] cbT;
	public double[][] cnT;
	public double[][] cT1;
	public double[][] b;
	public int[] basics;
	public int[] nonBasics;
	public double[][] N;
	public double[][] BiN;
	public int[] bb;
	public ArrayList<Integer> el;
	public int m,n;

	public Simplex(int m,int n,double[][] cT,double[][] cT1,double[][] a,double[][] b,boolean max){
		this.m=m;
		this.n=n;
		this.cT=cT;
		this.cT1=cT1;
		this.A=a;
		this.b=b;
		ph2=false;
		bb=new int[1];
		bb[0]=n-1;
		cbT=new double[1][m-1];
		wasMaxProblem=max;
		isMaxProblem=false;
		Bi=new double[m-1][m-1];
		Bib=new double[1][m-1];
		nonBasics=new int[n-m-1];
		basics=new int[m-1];
		BiN=new double[m-1][n-m-1];
		for(int i=1; i<n ; i++){
			A[0][i]=cT[i-1][0];
			if(isMaxProblem) A[0][i]*=-1;
		}
		A[0][n-1]=0;
		A[0][0]=1;
		el=new ArrayList<Integer>();
		for(int i=1 ; i<m ; i++) A[i][0]=0;
		for(int i=1 ; i<m ; i++) A[i][n-1]=b[i-1][0];
		//print(A);
		for(int i=1 ; i<n ; i++) if(Math.abs(A[0][i]) > DELTA ){
			el.add(new Integer(i));
			System.out.println(i);
		}
		for(Integer i : el) eliminate(i.intValue());

		print(A);
		boolean olmadi=false;
		boolean[] isBasic=new boolean[n];
		for(int k=1 ; k<m ; k++) {
			for (int j = 1 ; j < n-1 ; j++) {
				for (int i = 1; i < m; i++) {
					if(k!=i) {
						if (!(Math.abs(A[k][j] - 1) < DELTA && Math.abs(A[i][j]) < DELTA)) {
							olmadi = true;
						}
					}
				}
				if(!olmadi){
					basics[k-1]=j;
				}
				olmadi=false;
			}
		}
		for(int i=0 ; i<basics.length ; i++){
			isBasic[basics[i]]=true;
		}
		for(int i=1 , k=0 ; i<n-1 ; i++){
			if(!isBasic[i]){
				nonBasics[k++]=i;
			}
		}
		for(int i=0 ; i<nonBasics.length ; i++){
			System.out.println(nonBasics[i]);
		}
		Bi=invert(getArray(basics));
		N=getArray(nonBasics);
		BiN=mtMull(Bi,N);
		cbT=getrow0(basics);
		cnT=getrow0(nonBasics);
		System.out.println("cbT");
		Bib=mtMull(Bi,b);
		rhs=mtMull(cbT,Bib);
		XN=mtMull(cbT,BiN);
		mtSub(XN,cnT);
	}

	public int findNextCol(){
		double next=0;
		int index=-1;
		for(int i=0 ; i<XN[0].length ; i++ ){
			if(el.contains(i)) continue;
			if(isMaxProblem){
				if(next > XN[0][i]){
					next=XN[0][i];
					index=i;
				}
			}else{
				if(next < XN[0][i]){
					next=XN[0][i];
					index=i;
				}
			}
		}
		return index;
	}

	public boolean findRatios(int col){
		if (col == -1) return false;
		double rat=Double.MAX_VALUE;
		int  index=-1;
		for(int i=0 ; i< BiN[0].length ; i++){
			if(Bib[i][0]/BiN[i][col] > 0 && rat > Bib[i][0]/BiN[i][col]){
				rat=Bib[i][0]/BiN[i][col];
				//System.out.println(rat+" "+i);
				index=i;
			}
		}
		if(index==-1) return false;
		basics[index]=nonBasics[col];
		boolean[] isBasic=new boolean[n];
		//System.out.println();
		for(int i=0 ; i<basics.length ; i++){
			isBasic[basics[i]]=true;
		}
		for(int i=1 , k=0 ; i<n-1 ; i++){
			if(!isBasic[i]){
				nonBasics[k++]=i;
			}
		}

		return true;
	}

	public void solvePhase1(){
		while(findRatios(findNextCol())){
			Bi=invert(getArray(basics));
			//System.out.println("Bi");
			//print(Bi);
			N=getArray(nonBasics);
			BiN=mtMull(Bi,N);
			//System.out.println("BiN");
			//print(BiN);
			cbT=getrow0(basics);
			cnT=getrow0(nonBasics);
			System.out.println("cbT");
			print(cbT);
			//System.out.println("b");
			//print(b);
			Bib=mtMull(Bi,b);
			System.out.println("Bib");
			print(Bib);
			rhs=mtMull(cbT,Bib);
			System.out.println("rhs: "+rhs[0][0]);
			XN=mtMull(cbT,BiN);
			mtSub(XN,cnT);
			System.out.println("XN");
			print(XN);
		}
	}


	public void solvePhase2(){
		setArray(nonBasics,BiN);
		setArray(basics,mtMull(getArray(basics),Bi));
		setArray(bb,Bib);
		for(int i=1 ; i<n-1 ; i++) A[0][i]=cT1[i-1][0];
		A[0][n-1]-=rhs[0][0];
		print(A);
		nonBasics=new int[nonBasics.length-el.size()];
		System.out.println();
		boolean olmadi=false;
		boolean[] isBasic=new boolean[n-el.size()];
		for(int k=1 ; k<m ; k++) {
			for (int j = 1 ; j < n-1 ; j++) {
				for (int i = 1; i < m; i++) {
					if(k!=i) {
						if (!(Math.abs(A[k][j] - 1) < DELTA && Math.abs(A[i][j]) < DELTA)) {
							olmadi = true;
						}
					}
				}
				if(!olmadi){
					basics[k-1]=j;
				}
				olmadi=false;
			}
		}
		for(int i=0 ; i<basics.length ; i++){
			isBasic[basics[i]]=true;
			System.out.println(basics[i]);
		}
		for(int i=1 , k=0 ; i<n-1-el.size() ; i++){
			if(!isBasic[i]){
				nonBasics[k++]=i;
			}
		}
		for(int i=0 ; i<basics.length ; i++) if(basics[i]!=0) eliminate(basics[i]);
		print(A);
		isMaxProblem=wasMaxProblem;
		ph2=true;
		solvePhase1();
		System.out.println("Cevabimiz: "+(A[0][n-1]-rhs[0][0]));
		setArray(nonBasics,BiN);
		setArray(basics,mtMull(getArray(basics),Bi));
		print(Bib);
		setArray(bb,Bib);
		print(A);
	}



	public void eliminate(int col){
		double cons=A[0][col];
		for(int i=1 ; i<m ; i++) if(Math.abs(A[i][col]) > DELTA){
			for(int j=1 ; j<n ; j++){
				A[0][j]-=A[i][j]*cons;
			}
		}
		A[0][col]=0;
	}

	public double[][] getArray(int[] basis){
		double[][] result=new  double[m-1][basis.length];
		for(int i=0 ; i<result.length ; i++){
			for(int j=0 ; j<basis.length ; j++){
				result[i][j]=A[i+1][basis[j]];
			}
		}
		return result;
	}

	public void setArray(int[] basis,double[][] result){
		for(int i=0 ; i<result.length ; i++){
			for(int j=0 ; j<basis.length ; j++){
				A[i+1][basis[j]]=result[i][j];
			}
		}
	}

	public double[][] getrow0(int[] basis){
		double[][] result=new  double[1][basis.length];
		for(int j=0 ; j<basis.length ; j++){
			result[0][j]=A[0][basis[j]];
		}
		return result;
	}

	public double[][] invert(double a[][]) {
		int n = a.length;
		double x[][] = new double[n][n];
		double b[][] = new double[n][n];
		int index[] = new int[n];
		for (int i=0; i<n; ++i) b[i][i] = 1;

		gaussian(a, index);

		for (int i=0; i<n-1; ++i)
			for (int j=i+1; j<n; ++j)
				for (int k=0; k<n; ++k)
					b[index[j]][k] -= a[index[j]][i]*b[index[i]][k];

		for (int i=0; i<n; ++i) {
			x[n-1][i] = b[index[n-1]][i]/a[index[n-1]][n-1];
			for (int j=n-2; j>=0; --j) {
				x[j][i] = b[index[j]][i];
				for (int k=j+1; k<n; ++k) {
					x[j][i] -= a[index[j]][k]*x[k][i];
				}
				x[j][i] /= a[index[j]][j];
			}
		}
		return x;
	}

	public  void gaussian(double a[][],int index[]) {
		int n = index.length;
		double c[] = new double[n];

		for (int i=0; i<n; ++i) index[i] = i;
		for (int i=0; i<n; ++i) {
			double c1 = 0;
			for (int j=0; j<n; ++j) {
				double c0 = Math.abs(a[i][j]);
				if (c0 > c1) c1 = c0;
			}
			c[i] = c1;
		}
		int k = 0;
		for (int j=0; j<n-1; ++j) {
			double pi1 = 0;
			for (int i=j; i<n; ++i) {
				double pi0 = Math.abs(a[index[i]][j]);
				pi0 /= c[index[i]];
				if (pi0 > pi1) {
					pi1 = pi0;
					k = i;
				}
			}
			int itmp = index[j];
			index[j] = index[k];
			index[k] = itmp;
			for (int i=j+1; i<n; ++i) {
				double pj = a[index[i]][j]/a[index[j]][j];
				a[index[i]][j] = pj;
				for (int l=j+1; l<n; ++l)
					a[index[i]][l] -= pj*a[index[j]][l];
			}
		}
	}

	public double[][] mtMull(double[][] a,double[][] b){
		double[][] ar=new double[a.length][b[0].length];
		for(int i=0 ; i< a.length ; i++){
			for(int j=0 ; j< b[0].length ; j++){
				for(int k=0 ; k< b.length ; k++){
					ar[i][j]+=a[i][k]*b[k][j];
				}
			}
		}
		return ar;
	}

	public void mtSub(double[][] m1,double[][] m2){
		for(int i=0 ; i<m1.length ; i++){
			for(int j=0 ; j<m1[0].length ; j++){
				m1[i][j]-=m2[i][j];
				m1[i][j]*=-1;
			}
		}
	}

	public void print(double[][] ar){
		for(int i=0 ; i<ar.length ; i++){
			for(int j=0 ; j<ar[0].length ; j++){
				System.out.printf("%.2f ",ar[i][j]);
			}
			System.out.println();
		}
	}
	
}
