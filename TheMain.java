import java.util.*;
import java.io.*;

public class TheMain{
	static PrintWriter writer;
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		Scanner in=new Scanner(new File("input.txt")).useLocale(Locale.US);
		in.nextLine();
        String type=in.next(); in.nextLine();
        boolean max=type.equalsIgnoreCase("maximization");in.nextLine();
        in.nextLine();
		int n=in.nextInt()+2;
		in.nextLine();in.nextLine();in.nextLine();
		int m=in.nextInt()+1; in.nextLine();
		in.nextLine(); in.nextLine();
		double[][] cT0=new double[n-1][1];
        double[][] cT1=new double[n-1][1];
		double[][] A= new double[m][n];
		double[][] b=new double[m-1][1];

		for(int i=0 ; i<n-2 ; i++){
			cT0[i][0]=in.nextDouble();
		}
		in.nextLine(); in.nextLine();in.nextLine();
        for(int i=0 ; i<n-2 ; i++){
            cT1[i][0]=in.nextDouble();
        }
        in.nextLine(); in.nextLine();in.nextLine();
		for(int i=1 ; i<m ; i++){
			for(int j=1 ; j<n-1 ; j++){
				A[i][j]=in.nextDouble();
			}
			in.nextLine();
		}
		in.nextLine();in.nextLine();
		for(int i=1 ; i<m ; i++){
			b[i-1][0]=in.nextDouble();
		}



		Simplex simplex=new Simplex(m,n,cT0,cT1,A,b,max);
		writer = new PrintWriter("output.txt", "UTF-8");
		simplex.solvePhase1();
		simplex.solvePhase2();
		/*
		writer.println("2.1. B:\n");
		print(simplex.getArray(basics));
        writer.println("\n2.2. B~:\n");
        print(invert(simplex.getArray(basics)));
		writer.println("\n2.3. N:\n");
		print(simplex.getArray(nonBasics));
		simplex.goState(basics);
		writer.println("\n2.4. B~N:\n");
		print(simplex.getArray(nonBasics));
		writer.println("\n2.5. B~b:\n");
        print(simplex.getArray(bb));
        writer.println("\n2.6. CbTB~b:\n");
        print(simplex.getrow0(bb));
        writer.println("\n2.7. CbTB~N - CnT\n");
        print(simplex.getrow0(nonBasics));
        */
		writer.close();

    }

}
